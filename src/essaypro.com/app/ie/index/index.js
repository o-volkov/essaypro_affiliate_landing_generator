'use strict';

require('./inject');

const locale = 'ie';

const index = require('../../common/templates/index/index');
index.init(locale);
