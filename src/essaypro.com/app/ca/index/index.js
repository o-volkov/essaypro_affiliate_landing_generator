'use strict';

require('./inject');

const config = require('../../../config');

const locale = 'ca';

const index = require('../../common/templates/index/index');
index.init(locale);
