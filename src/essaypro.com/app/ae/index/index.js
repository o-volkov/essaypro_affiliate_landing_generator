'use strict';

require('./inject');

const locale = 'ae';

const index = require('../../common/templates/index/index');
index.init(locale);
