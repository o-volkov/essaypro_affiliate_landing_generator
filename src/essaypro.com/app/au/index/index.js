'use strict';

require('./inject');

const locale = 'au';

const index = require('../../common/templates/index/index');
index.init(locale);
