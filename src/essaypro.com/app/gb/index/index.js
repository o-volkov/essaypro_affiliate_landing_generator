'use strict';

require('./inject');

const locale = 'gb';

const index = require('../../common/templates/index/index');
index.init(locale);
