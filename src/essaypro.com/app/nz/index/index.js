'use strict';

require('./inject');

const locale = 'nz';

const index = require('../../common/templates/index/index');
index.init(locale);
