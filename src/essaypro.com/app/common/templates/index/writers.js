'use strict';

const $ = require('jquery');
const config = require('../../../../config');
const moment = require('moment');

export function init(locale) {
  const locale_link = locale != 'us' ? '/' + locale + '/' : '/';
  (function () {
    let $writersList = $('#writers-list');

    function renderWriter(writer) {
      let status = writer.writer_online ? 'online': 'offline';
      let shortDescription = writer.shortDescription.slice(0, 50) + (writer.shortDescription.length > 50 ? '...' : '');

      //TODO поменять на бэке, когда перейдем на https
      writer.avatarSrc = writer.avatarSrc.replace('http://', 'https://');

      var html = `
      <li class="writers-list-item">
        <div class="top-block__mobile" itemprop="employee" itemtype="http://schema.org/Person" itemscope="">
        <meta itemprop="homeLocation" content="New York" />
          <div class="left-block">
            <div class="avatar-block">
              <a href="${config.frontend_url}${locale_link}essay-writer.html?id=${writer.id}&tap_s=${TAP_S}" itemprop="url" class="ga-event" data-ga-category="home" data-ga-action="click" data-ga-label="home-body-6 Avatar click">
                `+(writer.avatarSrc ? `<img src="${writer.avatarSrc}" itemprop="image" alt="${writer.nickname}" class="avatar">` : '')+`
              </a>
            </div>
          </div>
          <div class="central-block central-block__top">
            <div class="name-block">
              <div class="name" itemprop="name">
                <a  href="${config.frontend_url}${locale_link}essay-writer.html?id=${writer.id}&tap_s=${TAP_S}" class="name-link ga-event" data-ga-category="home" data-ga-action="click" data-ga-label="home-body-6 Nickname click">${writer.nickname}</a>
                <span class="status-block">
                    <span class="status ${status}"></span>
                </span>
              </div>
            </div>
            <div class="description" itemprop="description">${shortDescription}</div>
          </div>
        </div>
        <div class="bottom-block__mobile">
          <div class="central-block central-block__bottom">
            <div class="order-block" itemscope itemtype="http://schema.org/OrderItem">
              <div class="stats-title">Orders completed</div>
              <div class="stats-value" itemprop="orderQuantity">${writer.stats.finishedOrdersCount}</div>
              <div class="stats-percent">(${writer.stats.completionsRate}%)</div>
            </div>
            <div class="rating-wrapper" itemscope itemtype="http://schema.org/Rating">
              <div class="stats-title">Rating</div>
              <meta itemprop="worstRating" content="1">
              <div class="stats-value" itemprop="bestRating">${writer.stats.ratingMark.slice(0, -1)}</div>
               <meta itemprop="bestRating" content="5">
              <div class="rating-bar" itemprop="starRating">
                <div class="rating-star active" style="width: ${writer.stats.ratingMark * 20}%"></div>
                <div class="rating-star"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="right-block">
          <div class="hire-block">
            <a rel="nofollow" href="${config.backend_url}/auth/registration?invite_writer=${writer.id}&tap_s=${TAP_S}" class="button hire-button" data-ga-category="writer" data-ga-action="click" data-ga-label="HIRE">Hire</a>
          </div>
        </div>
        <div class="rating-position-block">
          ` + (writer.stats.ratingPosition !== 0 ? `<div class="rating-position award-${writer.stats.ratingPosition}"></div>` : '')+`
        </div>
      </li>
        `;

      $writersList.append(html);
    }

    function loadWriters() {
      $.ajax({
        url: `${config.backend_url}/api/catalog`,
        method: 'GET',
        data: {limit: 6}
      }).done(function(data) {
        data.writers.forEach(function(writer) {
          renderWriter(writer);
        });
      })
    }

    loadWriters();
  })();
}
