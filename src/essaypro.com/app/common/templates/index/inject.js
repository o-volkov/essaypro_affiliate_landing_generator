'use strict';

const writersInject = require('../writers/inject');
// const feedbacksInject = require('../feedbacks/inject');
// const calcInject = require('../calc/inject_1');

module.exports = function() {
  // Vendors
  require('magnific-popup/dist/magnific-popup.css');

  // Ours
  require('./styles/home/backgrounds/_home_bgr.scss');
  require('./styles/home/backgrounds/_home_bgr.scss');
  require('./styles/home/backgrounds/_home_bgr__0320_less.scss');
  require('./styles/home/backgrounds/_home_bgr__0321_0568.scss');
  require('./styles/home/backgrounds/_home_bgr__0569_0767.scss');
  require('./styles/home/backgrounds/_home_bgr__0768_only.scss');
  require('./styles/home/backgrounds/_home_bgr__0769_1024.scss');
  require('./styles/home/backgrounds/_home_bgr__1025_1440.scss');
  require('./styles/home/backgrounds/_home_bgr__1441_1920.scss');
  require('./styles/home/backgrounds/_home_bgr__other.scss');

  require('./styles/home/_home.scss');
  require('./styles/home/_home__0401_0480.scss');
  require('./styles/home/_home__0480_less.scss');
  require('./styles/home/_home__0481_0568.scss');
  require('./styles/home/_home__0560_less.scss');
  require('./styles/home/_home__0569_0767.scss');
  require('./styles/home/_home__0767_less.scss');
  require('./styles/home/_home__0768_1000.scss');
  require('./styles/home/_home__0768_1024.scss');
  require('./styles/home/_home__0768_more.scss');
  require('./styles/home/_home__0941_more.scss');
  require('./styles/home/_home__1025_more.scss');
  require('./styles/home/_home__1441_more.scss');
  require('./styles/home/_home__other.scss');


  require('./styles/section/styles.scss');

  require('../../styles/components/avatar/styles.scss');
  require('../../styles/components/item/styles.scss');
  require('../../styles/components/calculator/styles.scss');
  // calcInject();
  // feedbacksInject();
  writersInject();
};
