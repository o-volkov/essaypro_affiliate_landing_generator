'use strict';

require('retinajs');
require('magnific-popup/dist/jquery.magnific-popup.min.js');

const calc = require('../calc/calc');
const writers = require('./writers');
const feedbacks = require('./feedbacks');
const section11 = require('./section-11');

var allPanels = $('.es-section__tab-panel');
allPanels.hide();
$('#th-1').addClass('active');
$('#tp-1').show();

$('.es-section__tab-heading').hover(
    function () {
      $('.es-section__tab-heading').removeClass('active');
      $(this).addClass('active');
      var currentId = $(this).attr('id');

      switch (currentId) {
        case 'th-1':
          allPanels.hide();
          $('#tp-1').show();
          break;
        case 'th-2':
          allPanels.hide();
          $('#tp-2').show();
          break;
        case 'th-3':
          allPanels.hide();
          $('#tp-3').show();
          break;
      }

    }
);

export function init(locale) {
  (function (locale) {
    calc.init(locale);
    writers.init(locale);
    feedbacks.init(locale);
    section11.init(locale);

    $(document).ready(function() {
      retinajs($('img[data-rjs]'));

      try {
        $('.with-popup').magnificPopup({
          type: 'image',
          closeOnContentClick: true,
          closeBtnInside: false,
          fixedContentPos: true,
          mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
          image: {
            verticalFit: true
          },
          zoom: {
            enabled: true,
            duration: 300 // don't foget to change the duration also in CSS
          }
        });
      } catch (e) {
      }
    });
  })(locale)
}
