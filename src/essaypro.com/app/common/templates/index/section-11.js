'use strict';

const $ = require('jquery');

export function init(locale) {
  (function (locale) {
    $('.es-section-11 .dropdown-menu li').click(function() {
      let text = $(this).text();
      let value = $(this).attr('data-calc-value');
      $(this).parents('.dropdown').find('.selected-value').html(text);
      $(this).parents('.dropdown').find('.selected-value').attr('data-calc-value', value);

      $('#service').val(
        parseInt($('#service_selected .selected-value').attr('data-calc-value'))
      );
      $('#type').val(
        parseInt($('#type_selected .selected-value').attr('data-calc-value'))
      );
      $('#deadline').val(
        parseInt($('#deadline_selected .selected-value').attr('data-calc-value'))
      );
      $('#pages').val(
        parseInt($('#pages_selected .selected-value').attr('data-calc-value'))
      );
    });


    $('#pages_increment').click(function () {
      let nextOption = document.getElementById('pages').selectedIndex + 1;
      let newValue = (nextOption == $('select#pages option').length) ? 1 : nextOption + 1;

      let el = $(`.es-section-11 .dropdown-menu[aria-labelledby="pages_selected"] li[data-calc-value="${newValue}"]`);
      $('#pages_selected .selected-value').html(el.text());
      $('#pages_selected .selected-value').attr('data-calc-value', el.attr('data-calc-value'));

      // $('select#pages option').eq(newValue).prop('selected', true);

      $('#pages').val(
        parseInt($('#pages_selected .selected-value').attr('data-calc-value'))
      );
    });

    $('#pages_decrement').click(function () {
      let prevOption = document.getElementById('pages').selectedIndex + 1;
      let newValue = (prevOption == 1) ? $('select#pages option').length : prevOption - 1;

      let el = $(`.es-section-11 .dropdown-menu[aria-labelledby="pages_selected"] li[data-calc-value="${newValue}"]`);
      $('#pages_selected').parents('.dropdown').find('.selected-value').html(el.text());
      $('#pages_selected').parents('.dropdown').find('.selected-value').attr('data-calc-value', el.attr('data-calc-value'));

      // $('select#pages option').eq(newValue).prop('selected', true);

      $('#pages').val(
        parseInt($('#pages_selected .selected-value').attr('data-calc-value'))
      );
    });
  })(locale);
}
