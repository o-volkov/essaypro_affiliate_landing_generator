'use strict';

const $ = require('jquery');
const config = require('../../../../config');
const moment = require('moment');
const avatar = require('./images/avatar__type_customers__30x30.svg');

export function init(locale) {
  const locale_link = locale != 'us' ? '/' + locale + '/' : '/';
  (function (locale_link) {
    let $feedbacksList = $('#feedbacks-list');
    let $feedbacksHeading = $('#feedbacksHeading');


    function renderAvgNum(avgNum) {
      var htmlAverageNum = `
        <div class="block-feedbacks" itemtype="http://schema.org/AggregateRating" itemscope itemprop="aggregateRating">
          <meta itemprop="itemReviewed" content="Custom Essay Writing Service">
          <div class="es-section__block__heading--stars item-reviews__customers-stat feedback_top--stat">
            <div class="item-reviews__customers-stat feedback_top--stat">
              <div class="stat-writers__item">
                 <meta itemProp="worstRating" content="1"/>
                <span class="value" itemprop="ratingValue">4.9</span>
                <meta itemProp="bestRating" content="5"/>
                <div class="rating-bar">
                  <div class="rating-star active" style="width: 95%"></div>
                  <div class="rating-star"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="average_rating--value" >Avg rating for <span id="rateAvgNum" itemprop="reviewCount" class="number">${avgNum}</span> reviews
          </div>
        </div>
`;
      $feedbacksHeading.append(htmlAverageNum);
    }


    function renderFeedback(feedback) {
      let status = feedback.writer_online ? 'online': 'offline';
      let feedbackDateAgo = moment(feedback.date).fromNow();

      var html = `
<li            class="item-reviews" itemprop="review" itemscope itemtype="http://schema.org/Review">
              <div class="item-reviews__customers-info" itemprop="author" itemscope itemtype="http://schema.org/Person">
              <meta itemprop="homeLocation" content="New York" />
                <a href="#" class="link">
                  <div class="avatar type__reviews size__small">
                    <img src="${avatar}" itemprop="image" class="avatar-image" alt="">
                  </div>
                  <div class="title" itemprop="name">Customer ${feedback.customer_id}</div>
                </a>
                <div class="date">${feedbackDateAgo}</div>
              </div>
              <div class="item-reviews__customers-stat" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                <div class="stat-writers__item">
                <meta itemprop="worstRating" content = "1"/>
                  <span class="value" itemprop="ratingValue">${feedback.mark}</span>
                  <meta itemprop="bestRating" content="5"/>
                  <div class="rating-bar">
                    <div class="rating-star active" style="width: ${feedback.mark * 20}%"></div>
                    <div class="rating-star"></div>
                  </div>
                </div>
              </div>
              <div class="item-reviews__customers-comment" itemprop="reviewBody">
                <p>${feedback.text}</p>
              </div>
              <div class="item-reviews__customers-category" itemscope itemtype="http://schema.org/Product">
                <span class="category" itemprop="name">
                  Essay <span class="essay-id">${feedback.order_id}</span> on <a href="${locale_link}custom-essay-writers.html#${feedback.order_subject}" class="link">${feedback.order_subject}</a>
                </span>
              </div>
              <div class="item-reviews__writers-info">
                <a  itemprop="url" href="${config.frontend_url}${locale_link}essay-writer.html?id=${feedback.writer_id}&tap_s=${TAP_S}" class="link">
                <div itemprop="itemReviewed" itemscope itemtype="http://schema.org/Thing">
                  <span itemprop="name">${feedback.writer_name}</span>
                  <i class="status ${status}"></i>
                  </div>
                </a>
              </div>
            </li>
    `;
      $feedbacksList.append(html);
    }



    function loadFeedbacks() {
      $.ajax({
        url: `${config.backend_url}/api/feedbacks`,
        method: 'GET',
        data: { limit: 6 }
      }).done(function(data) {
        renderAvgNum(data.countAll);
        data.data.forEach(function(feedback) {
          renderFeedback(feedback);
        });
      });
    }

    loadFeedbacks();
  })(locale_link);
}



