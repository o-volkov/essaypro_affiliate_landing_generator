'use strict';

module.exports = function() {
  require('./styles/feedbacks/_feedbacks.scss');
  require('./styles/feedbacks/_feedbacks__0421_more.scss');
  require('./styles/feedbacks/_feedbacks__0767_less.scss');
  require('./styles/feedbacks/_feedbacks__0768_0920.scss');
  require('../../styles/components/heading/styles.scss');

  require('../../styles/components/avatar/styles.scss');
  require('../../styles/components/item/styles.scss');
  require('../../styles/components/page/styles.scss');
};
