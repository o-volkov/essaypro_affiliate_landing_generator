'use strict';

module.exports = function() {
  require('./styles/writers/_writers.scss');
  require('./styles/writers/_writers__1__0767_less.scss');
  require('./styles/writers/_writers__1__0560_less.scss');
  require('./styles/writers/_writers__1__0768_more.scss');
  require('./styles/writers/_writers__1__0961_more.scss');
  require('./styles/writers/_writers__1__1200_more.scss');
  require('./styles/writers/_writers__2__0481_0568.scss');
  require('./styles/writers/_writers__2__0560_less.scss');
  require('./styles/writers/_writers__2__0561_0767.scss');
  require('./styles/writers/_writers__2__0768_0920.scss');
  require('./styles/writers/_writers__2__0921_0960.scss');

  require('../../styles/components/heading/styles.scss');
  require('../../styles/components/page/styles.scss');
};
