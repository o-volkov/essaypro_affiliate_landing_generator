'use strict';

module.exports = function () {

    require('material-design-lite/src/material-design-lite.scss');
    require('bootstrap-sass/assets/stylesheets/_bootstrap.scss');

    require('./styles/vendor/material-modal.min.css');

    require('./styles/vendor/accordion.min.css');
    require('./styles/form/form.scss');

    require('./styles/index.scss');
    require('./styles/vendor.scss');

    require('./styles/base/_base.scss');
    require('./styles/base/_default.scss');
    require('./styles/base/_mixins.scss');
    require('./styles/base/container/_container__md.scss');
    require('./styles/base/container/_container__sm.scss');
    require('./styles/base/container/_container__1200.scss');

    require('./styles/regions/header/header.scss');
    require('./styles/regions/title/title.scss');
    require('./styles/regions/menu/menu.scss');
    require('./styles/regions/side_nav/side_nav.scss');
    require('./styles/regions/mdl_side_nav/mdl_side_nav.scss');
    require('./styles/regions/content/_content__0768_more.scss');

    require('./styles/regions/layout_global/layout_global.scss');
    require('./styles/regions/footer/styles.scss');

    require('./styles/components/auth/auth.scss');
    require('./styles/components/content_header/_content_header.scss');
    require('./styles/components/heading/_heading.scss');
    require('./styles/components/heading/_heading__0400_less.scss');
    require('./styles/components/heading/_heading__0767_less.scss');

    require('./styles/components/calculator/styles.scss');
    require('./styles/components/avatar/styles.scss');
    require('./styles/components/item/styles.scss');

    require('./styles/components/content_list/_content_list.scss');
    require('./styles/components/content_list/_content_list__0767_less.scss');
    require('./styles/components/content_list/_content_list__0768_more.scss');
    require('./styles/components/content_list/_content_list__1025_more.scss');
    require('./styles/components/content_footer/_content_footer.scss');
    require('./styles/components/responsive_menu/_responsive_menu.scss');
    require('./styles/components/responsive_menu/_responsive_menu__0480_less.scss');
    require('./styles/components/responsive_menu/_responsive_menu__1000_less.scss');
    require('./styles/components/writers_menu/_writers_menu.scss');
    require('./styles/components/writers_menu/_writers_menu__0767_less.scss');
    require('./styles/components/writers_menu/_writers_menu__0768_more.scss');
    require('./styles/components/rating_bar/_rating_bar.scss');
    require('./styles/components/spinner/_spinner.scss');

    require('./styles/components/submenu/styles.scss');

    require('./styles/components/lang/_lang.scss');
    require('./styles/components/lang/_lang__0568_less.scss');
    require('./styles/components/lang/_lang__0767_less.scss');

    require('./styles/components/button/styles.scss');
    require('./styles/components/icon/styles.scss');
    require('./styles/components/nav/styles.scss');

    require('./styles/theme/theme.scss');
    require('./styles/theme/theme_container.scss');


    require('./styles/temp/main_raw.scss');
    require('./styles/temp/fix.scss');
    require('./styles/viewport.scss');

};
