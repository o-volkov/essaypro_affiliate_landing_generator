'use strict';


const $ = require('jquery');

// require('material-design-lite');
require('./scripts/vendor/bootstrap.min.js');
require('./scripts/vendor/device.min.js');
require('./scripts/authorization');
require('./locale');
const config = require('./../../config');

let inject = require('./inject');
inject();

//TODO переписать этот ужас
// Код для считываения урлы и подмены параметров  калькуляторе
function getUrlChangeCalcParam() {
    var checkServiceParam,
        checkPagesParam,
        checkDeadlineParam;

    var arr = [];
    var arr1 = '';
    var url = window.location.search;
    var url1 = window.location.search;

    for (var l = 1; l < url1.length; l++) {
        arr1 = arr1 + url1[l].replace(/[-!$%^&*()+={0-9}|~`{}\[\]:";'<>?,.\/]/g,'');
        if (arr1 == 'order_service') {
            var orderServiceParamNum = url.match(/(order_service=\d+).*$/)[1];
            checkServiceParam = orderServiceParamNum;
            arr += checkServiceParam;
            orderServiceParamNum = orderServiceParamNum.replace(/order_service=/, '');
            arr1 = '';
        } else if (arr1 == 'order_deadline') {
            var orderDeadlineParamNum = url.match(/(order_deadline=\d+).*$/)[1];
            checkDeadlineParam = orderDeadlineParamNum;
            arr += checkDeadlineParam;
            orderDeadlineParamNum = orderDeadlineParamNum.replace(/order_deadline=/, '');
            arr1 = '';
        } else if (arr1 == 'order_pages') {
            var orderPagesParamNum = url.match(/(order_pages=\d+).*$/)[1];
            checkPagesParam = orderPagesParamNum;
            arr += checkPagesParam;
            orderPagesParamNum = orderPagesParamNum.replace(/order_pages=/, '');
            arr1 = '';
        }
    }

    var paramArr = '';
    for (var i = 0; i < arr.length; i++) {
        paramArr = paramArr + arr[i];
        if (checkServiceParam == paramArr) {
            if (orderServiceParamNum == 1) {
                document.getElementById('calculator_service_selected_1').getElementsByTagName('span')[0].innerHTML = 'Writing';
                document.getElementById('calculator_service_selected_1').getElementsByTagName('span')[0].dataset.calcValue = orderServiceParamNum;
            } else if (orderServiceParamNum == 2) {
                document.getElementById('calculator_service_selected_1').getElementsByTagName('span')[0].innerHTML = 'Rewriting';
                document.getElementById('calculator_service_selected_1').getElementsByTagName('span')[0].dataset.calcValue = orderServiceParamNum;
            } else if (orderServiceParamNum == 3) {
                document.getElementById('calculator_service_selected_1').getElementsByTagName('span')[0].innerHTML = 'Editing';
                document.getElementById('calculator_service_selected_1').getElementsByTagName('span')[0].dataset.calcValue = orderServiceParamNum;
            }
            paramArr = '';
        } else if (checkPagesParam == paramArr) {
            document.getElementById('calculator_pages_selected_1').getElementsByTagName('span')[0].innerHTML = orderPagesParamNum + ' pages';
            document.getElementById('calculator_pages_selected_1').getElementsByTagName('span')[0].dataset.calcValue = orderServiceParamNum;
            paramArr = '';
        } else if (checkDeadlineParam == paramArr) {
            if (orderDeadlineParamNum == 0 ) {
                document.getElementById('calculator_deadline_selected_1').getElementsByTagName('span')[0].innerHTML = '6 hours';
                document.getElementById('calculator_deadline_selected_1').getElementsByTagName('span')[0].dataset.calcValue = orderDeadlineParamNum;
            } else if(orderDeadlineParamNum == 6) {
                document.getElementById('calculator_deadline_selected_1').getElementsByTagName('span')[0].innerHTML = '12 hours';
                document.getElementById('calculator_deadline_selected_1').getElementsByTagName('span')[0].dataset.calcValue = orderDeadlineParamNum;
            } else if (orderDeadlineParamNum == 12 || orderDeadlineParamNum == 24) {
                document.getElementById('calculator_deadline_selected_1').getElementsByTagName('span')[0].dataset.calcValue = orderDeadlineParamNum;
                orderDeadlineParamNum = orderDeadlineParamNum / 12;
                document.getElementById('calculator_deadline_selected_1').getElementsByTagName('span')[0].innerHTML = orderDeadlineParamNum + ' days';

            } else if (orderDeadlineParamNum == 48) {
                document.getElementById('calculator_deadline_selected_1').getElementsByTagName('span')[0].dataset.calcValue = orderDeadlineParamNum;
                document.getElementById('calculator_deadline_selected_1').getElementsByTagName('span')[0].innerHTML = '3 days';

            } else if (orderDeadlineParamNum == 72) {
                document.getElementById('calculator_deadline_selected_1').getElementsByTagName('span')[0].innerHTML = '5 days';
                document.getElementById('calculator_deadline_selected_1').getElementsByTagName('span')[0].dataset.calcValue = orderDeadlineParamNum;
            } else if (orderDeadlineParamNum == 120) {
                document.getElementById('calculator_deadline_selected_1').getElementsByTagName('span')[0].innerHTML = '7 days';
                document.getElementById('calculator_deadline_selected_1').getElementsByTagName('span')[0].dataset.calcValue = orderDeadlineParamNum;

            } else if (orderDeadlineParamNum == 168) {
                document.getElementById('calculator_deadline_selected_1').getElementsByTagName('span')[0].innerHTML = '10 days';
                document.getElementById('calculator_deadline_selected_1').getElementsByTagName('span')[0].dataset.calcValue = orderDeadlineParamNum;
            } else if (orderDeadlineParamNum == 336) {
                document.getElementById('calculator_deadline_selected_1').getElementsByTagName('span')[0].innerHTML = '2 weeks';
                document.getElementById('calculator_deadline_selected_1').getElementsByTagName('span')[0].dataset.calcValue = orderDeadlineParamNum;
            } else if (orderDeadlineParamNum == 720) {
                document.getElementById('calculator_deadline_selected_1').getElementsByTagName('span')[0].innerHTML = '1 month';
                document.getElementById('calculator_deadline_selected_1').getElementsByTagName('span')[0].dataset.calcValue = orderDeadlineParamNum;
            } else if (orderDeadlineParamNum == 1440) {
                document.getElementById('calculator_deadline_selected_1').getElementsByTagName('span')[0].innerHTML = '2 months';
                document.getElementById('calculator_deadline_selected_1').getElementsByTagName('span')[0].dataset.calcValue = orderDeadlineParamNum;
            }
            paramArr = '';
        }

    }

}

if (window.location.search) {
    try {
        getUrlChangeCalcParam();
    } catch (e) {
    }

}

//CANONICAL
// function addCanonicalHttps() {
//     var link = document.createElement('link');
//
//     if (!window.location.search) {
//         var url = window.location.origin + window.location.pathname;
//         link.setAttribute('rel', 'canonical');
//         link.setAttribute('href', url);
//
//         document.head.appendChild(link)
//     }
//
// }
//
// try {
//     addCanonicalHttps();
// } catch (e) {
//
// }

// $('.scroll-on-click').on('click', function () {
//     let $body = $('body');
//     $body.animate({scrollTop: 0}, 'fast');
// });

$('#sideNavLeft').click(function (e) {
    e.preventDefault();
    if ($('.mdl-layout__drawer-left').hasClass('active')) {
        $('.mdl-layout__drawer-left').removeClass('active');
    } else {
        $('.mdl-layout__drawer-left').addClass('active');
    }
});

$('#sideNavRight').click(function (e) {
    e.preventDefault();
    if ($('.mdl-layout__drawer-right').hasClass('active')) {
        $('.mdl-layout__drawer-right').removeClass('active');
    } else {
        $('.mdl-layout__drawer-right').addClass('active');
    }
});

let $subnav = $('.subnav');
let $body = $('body');

$('#subnav').on('click', function () {
    $subnav.addClass('open');
    return false;
});

$('#back').on('click', function () {
    $subnav.removeClass('open');
    return false;
});

var $mdlButtonLeft = $('.side-nav__button-left');
var $mdlButtonRight = $('.side-nav__button-right');
var $mdlDrawerLeft = $('.mdl-layout__drawer-left');
var $mdlDrawerRight = $('.mdl-layout__drawer-right');
var $mdlOverlayLeft = $('.mdl-layout__obfuscator-left');
var $mdlOverlayRight = $('.mdl-layout__obfuscator-right');

setInterval(function () {
    if (!$mdlDrawerLeft.hasClass('active')) {
        $subnav.removeClass('open');
    }
}, 1000);

$mdlButtonLeft.on('click', function () {
    if ($mdlButtonLeft.hasClass('active')) {
        $mdlButtonLeft.removeClass('active');
        $mdlOverlayLeft.css({'visibility': 'hidden'});
        $body.css({'overflow': 'visible'});
    } else {
        $mdlButtonRight.removeClass('active');
        $mdlButtonLeft.addClass('active');
        $mdlDrawerRight.removeClass('active');
        $mdlOverlayRight.css({'visibility': 'hidden'});
        $mdlOverlayLeft.css({'visibility': 'visible'});
        $body.css({'overflow': 'hidden'});
    }
});

$mdlButtonRight.on('click', function () {
    if ($mdlButtonRight.hasClass('active')) {
        $mdlButtonRight.removeClass('active');
        $mdlOverlayRight.css({'visibility': 'hidden'});
        $body.css({'overflow': 'visible'});
    } else {
        $mdlButtonLeft.removeClass('active');
        $mdlButtonRight.addClass('active');
        $mdlDrawerLeft.removeClass('active');
        $mdlOverlayLeft.css({'visibility': 'hidden'});
        $mdlOverlayRight.css({'visibility': 'visible'});
        $body.css({'overflow': 'hidden'});
    }

});

$mdlOverlayLeft.click(function () {
    if ($mdlDrawerLeft.hasClass('active')) {
        $mdlButtonLeft.removeClass('active');
        $mdlDrawerLeft.removeClass('active');
        $mdlOverlayLeft.css({'visibility': 'hidden'});
        $mdlOverlayRight.css({'visibility': 'hidden'});
        $body.css({'overflow': 'visible'});
    }
});

$mdlOverlayRight.click(function () {
    if ($mdlDrawerRight.hasClass('active')) {
        $mdlButtonRight.removeClass('active');
        $mdlDrawerRight.removeClass('active');
        $mdlOverlayLeft.css({'visibility': 'hidden'});
        $mdlOverlayRight.css({'visibility': 'hidden'});
        $body.css({'overflow': 'visible'});
    }
});

$('.submenu-target').click(function (e) {
    $(this).parent().toggleClass('active');
    e.stopPropagation();
});

$(window).click(function() {
    if ($('.submenu-target').parent().hasClass('active')) {
        $('.submenu-target').parent().removeClass('active');
    }
    if ($('.langmenu-target').parent().hasClass('active')) {
      $('.langmenu-target').parent().removeClass('active');
    }
});

$('.langmenu-target').click(function (e) {
  $(this).parent().toggleClass('active');
  e.stopPropagation();
});
