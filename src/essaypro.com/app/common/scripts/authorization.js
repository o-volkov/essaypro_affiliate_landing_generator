'use strict';

const $ = require('jquery');

const config = require('../../../config');

$(document).ready(function() {
  (function checkAuthorization() {
    try {
      localStorage.setItem('X-Entry-url', document.location.href);
      localStorage.setItem('X-Referrer-url', document.referrer);
      localStorage.setItem('X-Storage-Allowed', true);

      localStorage.setItem('allowed', 'allowed');
      localStorage.removeItem('allowed');
    } catch (e) {}

    return $.ajax({
        method: 'GET',
        url: `${config.backend_url}/api/user/me`,
        headers: {
          'X-Entry-url': localStorage.getItem('X-Entry-url'),
          'X-Referrer-url': localStorage.getItem('X-Referrer-url'),
          'X-Storage-Allowed': localStorage.getItem('X-Storage-Allowed')
        },
        xhrFields: {
          withCredentials: true
        }
      })
      .done(() => {
        window.location.href = config.backend_url;
      });
  }());
});
