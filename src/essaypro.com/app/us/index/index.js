'use strict';

require('./inject');

const locale = 'us';

const index = require('../../common/templates/index/index');
index.init(locale);
