module.exports = {
  frontend_url: 'https://essaypro.com',
  site_url: 'https://essaypro.com',
  backend_url: 'https://app.essaypro.com',
  app_url: 'https://app.essaypro.com'
};
