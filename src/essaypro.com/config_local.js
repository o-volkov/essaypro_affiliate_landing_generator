module.exports = {
  frontend_url: 'https://essaypro.com',
  site_url: 'http://127.0.0.1:5000',
  backend_url: 'http://127.0.0.1:5000',
  app_url: 'https://app.essaypro.com'
};
