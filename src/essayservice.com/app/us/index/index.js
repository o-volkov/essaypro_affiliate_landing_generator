'use strict';

require('./inject');

const locale = 'us';

const index = require('../../common/templates/index/index');


// common.init(locale);

const calc1 = require('../../common/templates/calc/calc');
calc1.init(locale, '.calculator-theme__white', 1);

const calc2 = require('../../common/templates/calc/calc');
calc2.init(locale, '.calculator-theme__white', 2);

