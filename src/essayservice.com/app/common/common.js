'use strict';

require('./scripts/vendor/bootstrap.min.js');
require('./locale');
const calc = require('./templates/calc/calc');


if (location.pathname.length > 1 && !location.pathname.match(/index.html/)) {
    document.getElementById('header').className = 'header__white';
    $('[data-logo="white"]').toggleClass('logo_hidden');
    $('[data-logo="black"]').removeClass('logo_hidden');
}

export function init(locale) {
    (function (locale) {
        calc.init(locale);
    })(locale)
}


let inject = require('./inject');

inject();

$('#sideNavLeft').click(function (e) {
    e.preventDefault();
    if ($('.mdl-layout__drawer-left').hasClass('active')) {
        $('.mdl-layout__drawer-left').removeClass('active');
    } else {
        $('.mdl-layout__drawer-left').addClass('active');
        $("body").append("<div class='mfp-bg'></div>")
    }
});

$('#sideNavClose').click(function (e) {
    e.preventDefault();
    if ($('.mdl-layout__drawer-left').hasClass('active')) {
        $('.mdl-layout__drawer-left').removeClass('active');
        $("body").find(".mfp-bg").remove();
    } else {
        $('.mdl-layout__drawer-left').addClass('active');
        $("body").append("<div class='mfp-bg'></div>")
    }
});

let $subnav = $('.subnav');
let $body = $('body');

$('#subnav').on('click', function () {
    $subnav.addClass('open');
    return false;
});

$('#back').on('click', function () {
    $subnav.removeClass('open');
    return false;
});

var $mdlButtonLeft = $('.side-nav__button-left');
var $mdlButtonRight = $('.side-nav__button-right');
var $mdlDrawerLeft = $('.mdl-layout__drawer-left');
var $mdlDrawerRight = $('.mdl-layout__drawer-right');
var $mdlOverlayLeft = $('.mdl-layout__obfuscator-left');
var $mdlOverlayRight = $('.mdl-layout__obfuscator-right');

setInterval(function () {
    if (!$mdlDrawerLeft.hasClass('active')) {
        $subnav.removeClass('open');
    }
}, 1000);

$mdlButtonLeft.on('click', function () {
    if ($mdlButtonLeft.hasClass('active')) {
        $mdlButtonLeft.removeClass('active');
        $mdlOverlayLeft.css({'visibility': 'hidden'});
        $body.css({'overflow': 'visible'});
    } else {
        $mdlButtonRight.removeClass('active');
        $mdlButtonLeft.addClass('active');
        $mdlDrawerRight.removeClass('active');
        $mdlOverlayRight.css({'visibility': 'hidden'});
        $mdlOverlayLeft.css({'visibility': 'visible'});
        $body.css({'overflow': 'hidden'});
    }
});

$mdlOverlayLeft.click(function () {
    if ($mdlDrawerLeft.hasClass('active')) {
        $mdlButtonLeft.removeClass('active');
        $mdlDrawerLeft.removeClass('active');
        $mdlOverlayLeft.css({'visibility': 'hidden'});
        $mdlOverlayRight.css({'visibility': 'hidden'});
        $body.css({'overflow': 'visible'});
    }
});

$('.submenu-target').click(function (e) {
    $(this).parent().toggleClass('active');
    e.stopPropagation();
});

$(window).click(function () {
    if ($('.submenu-target').parent().hasClass('active')) {
        $('.submenu-target').parent().removeClass('active');
    }
    if ($('.langmenu-target').parent().hasClass('active')) {
        $('.langmenu-target').parent().removeClass('active');
    }
});

$('.langmenu-target').click(function (e) {
    $(this).parent().toggleClass('active');
    e.stopPropagation();
});
