$(function () {
    /*
     $('.toggle_nav-mob').on('click', function () {
     $('#wrapper').toggleClass('show-nav');
     $('#wrapper').toggleClass('wrapper_isMobile');
     $('.nav_mob-i').toggleClass('onClose_i');
     }); /// ДЛЯ БУРГЕРА*/

    $('#learn').on('click', function (event) {
        event.stopPropagation();
        $('.mdl-menu__container').toggleClass('is-visible');
    }); /// ДЛЯ ДРОПДАУНА В МЕНЮ


    $(document).on('scroll', function () {
        var firstSection = $('.section-1');
        var isModified = $(this).scrollTop() - firstSection.height() >= 0;

        $('[data-logo="white"]').toggleClass('logo_hidden', isModified);
        $('[data-logo="black"]').toggleClass('logo_hidden', !isModified);
        $('#header').toggleClass('header__white', isModified);
    }); /// ДЛЯ СМЕНЫ ЛОГО НА ГЛАВНОЙ СТРАНИЦЫ ПРИ СКРОЛЛЕ

    $(window).on('resize', function () {
        var width = $(window).width();

        if ($(this).width() <= 540) {
            $(document).on('scroll', function () {
                var firstSection = $('.section-1');
                var isModified = $(this).scrollTop() - firstSection.height() >= 0;

                $('[data-logo="white"]').toggleClass('logo_hidden', isModified);
                $('[data-logo="black"]').toggleClass('logo_hidden', !isModified);
                $('#header').toggleClass('header__white', isModified);
            }); /// ДЛЯ СМЕНЫ ЛОГО НА ГЛАВНОЙ СТРАНИЦЫ ПРИ СКРОЛЛЕ
        }

    });

    $(document).on('click', function () {
        $('.mdl-menu__container').removeClass('is-visible');
    }); /// ДЛЯ ДРОПДАУНА В МЕНЮ

});
