module.exports = {
  frontend_url: 'https://essayservice.com',
  site_url: 'https://essayservice.com',
  backend_url: 'https://app.essayservice.com',
  app_url: 'https://app.essayservice.com'
};
