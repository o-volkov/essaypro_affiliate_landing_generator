module.exports = {
  frontend_url: 'https://essayservice.com',
  site_url: 'http://127.0.0.1:5000',
  backend_url: 'http://127.0.0.1:5000',
  app_url: 'https://app.essayservice.com'
};
