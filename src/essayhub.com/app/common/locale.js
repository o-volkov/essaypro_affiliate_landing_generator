function load(url, method, callback) {
    var xhr;

    if (typeof XMLHttpRequest !== 'undefined') {
        xhr = new XMLHttpRequest();
    } else {
        var versions = ["MSXML2.XmlHttp.5.0",
            "MSXML2.XmlHttp.4.0",
            "MSXML2.XmlHttp.3.0",
            "MSXML2.XmlHttp.2.0",
            "Microsoft.XmlHttp"];

        for (var i = 0, len = versions.length; i < len; i++) {
            try {
                xhr = new ActiveXObject(versions[i]);
                break;
            }
            catch(e){}
        }
    }

    xhr.onreadystatechange = ensureReadiness;

    function ensureReadiness() {
        if (xhr.readyState < 4) {
            return;
        }

        if (xhr.status !== 200) {
            return;
        }

        if (xhr.readyState === 4) {
            callback(xhr);
        }
    }

    xhr.open(method, url, true);
    xhr.send('');
}

function checkLocale() {
    try {
        if (localStorage.getItem('language')) {
            return;
        }
    } catch (e) {
        return;
    }

    var languages = ['au', 'ae', 'ca', 'gb', 'in', 'nz'];
    var language = 'en';
    load('https://echo.essaypro.com/', 'GET', function(xhr) {
        var json = JSON.parse(xhr.responseText);
        switch (json.country) {
            case 'AU':
                language = 'au';
                break;
            case 'AE':
                language = 'ae';
                break;
            case 'CA':
                language = 'ca';
                break;
            case 'IE':
                language = 'ie';
                break;
            case 'GB':
                language = 'gb';
                break;
            case 'NZ':
                language = 'nz';
                break;
            default:
                language = 'en';
        }

        try {
            localStorage.setItem('language', language);
        } catch (e) {}

        var parser = document.createElement('a');
        parser.href = window.location.pathname;

        var paths = parser.pathname.split('/');
        paths.shift();

        if (languages.indexOf(paths[0]) === -1) {
            if (languages.indexOf(paths[0]) >= 0 || languages.indexOf(language) >= 0) {
                window.location.href = window.location.origin + '/' + language + window.location.pathname +  window.location.hash + window.location.search;
            }
        }
    });
}

checkLocale();
