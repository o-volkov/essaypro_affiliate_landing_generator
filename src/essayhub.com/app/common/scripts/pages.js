$(function () {
    $('.toggle_nav-mob').on('click', function () {
        $('#wrapper').toggleClass('show-nav');
        $('#wrapper').toggleClass('wrapper_isMobile');
        $('.nav_mob-i').toggleClass('onClose_i');

    });

    $('#learn').on('click', function (event) {
        event.stopPropagation();
        $('.mdl-menu__container').toggleClass('is-visible');
    });
});