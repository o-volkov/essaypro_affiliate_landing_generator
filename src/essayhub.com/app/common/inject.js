module.exports = function () {
  require('./styles/header/styles/common1.scss');

  require('./styles/header/styles/styles.scss');

  require('./styles/calculator/styles.scss');

  require('./styles/index/section_1/styles/styles.scss');
  require('./styles/index/section_2/styles/styles.scss');
  require('./styles/index/section_3/styles/styles.scss');
  require('./styles/index/section_4/styles/styles.scss');
  require('./styles/index/section_5/styles/styles.scss');
  require('./styles/index/section_6/styles/styles.scss');
  require('./styles/index/section_7/styles/styles.scss');
  require('./styles/index/section_8/styles/styles.scss');
  require('./styles/section_footer/styles/styles.scss');

  // require('./styles/about_us/styles/styles.scss');
  // require('./styles/how_it_works/styles/styles.scss');
  // require('./styles/writers/styles/styles.scss');
  // require('./styles/feedback/styles/styles.scss');
  // require('./styles/service_faq/styles/styles.scss');
  // require('./styles/become_a_writer/styles/styles.scss');
  // require('./styles/privacy_policy/styles/styles.scss');
  // require('./styles/terms_conditions/styles/styles.scss');
  // require('./styles/custom_essay_writing/styles/styles.scss');
  // require('./styles/dissertation_writing_services/styles/styles.scss');

  require('./styles/common_templates/styles.scss');

  require('./styles/footer/styles/styles.scss');

};
