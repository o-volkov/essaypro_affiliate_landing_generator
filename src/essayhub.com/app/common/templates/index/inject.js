'use strict';

module.exports = function() {
    require('./section_1/styles/styles.scss');
    require('./section_2/styles/styles.scss');
    require('./section_3/styles/styles.scss');
    require('./section_4/styles/styles.scss');
    require('./section_5/styles/styles.scss');
    require('./section_6/styles/styles.scss');
    require('./section_7/styles/styles.scss');
    require('./section_8/styles/styles.scss');
};
