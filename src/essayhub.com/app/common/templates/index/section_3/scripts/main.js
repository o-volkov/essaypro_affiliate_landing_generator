'use strict';

function hasClass(target, className) {
    return new RegExp('(\\s|^)' + className + '(\\s|$)').test(target.className);
}

function changeImage() {
    var dataOpen = [].slice.call(document.getElementsByClassName('section-3')[0].querySelectorAll('[data-open]')),
        dataText = [].slice.call(document.getElementsByClassName('section-3')[0].querySelectorAll('[data-text]')),
        dataImage = [].slice.call(document.getElementsByClassName('section-3')[0].querySelectorAll('[data-img]'));


    var elementsArr = [];
    elementsArr = dataOpen.concat(dataImage, dataText);

    dataImage[0].play();
    dataImage.forEach(function (elem) {
        elem.controls = false;
    });

    dataOpen.forEach(function (elem, index) {
        elem.addEventListener('click', function () {
            elementsArr.forEach(function (elem) {
                if (hasClass(elem, 'text__flex__list__item')) {
                    elem.getElementsByClassName('arrowshow')[0].classList.remove('arrowUp');
                } else {
                    elem.classList.add('hidden');
                }
            });

            dataText[index].classList.remove('hidden');
            elem.getElementsByClassName('arrowshow')[0].classList.add('arrowUp');
            dataImage[index].classList.remove('hidden');
            dataImage[index].currentTime = 0;
            dataImage[index].play();
        }, false);
    });


}
changeImage();
