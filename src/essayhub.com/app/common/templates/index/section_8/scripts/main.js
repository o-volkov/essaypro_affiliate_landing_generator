(function () {

    function hasClass(target, className) {
        return new RegExp('(\\s|^)' + className + '(\\s|$)').test(target.className);
    }

    var dataOpen = [].slice.call(document.getElementsByClassName('section-8')[0].querySelectorAll('[data-open]'));
    var dataText;
    var dataTextDynamic;
    var arrDataText;
    var activeBlockNum = 0;

    //Responsive
    if (window.innerWidth >= '921') {
        dataText = document.getElementsByClassName('section-8')[0].querySelectorAll('[data-text]');
        dataTextDynamic = document.getElementsByClassName('section-8')[0].querySelectorAll('[data-textmobile]');
        blockChange();
        blockChangeMain();
    } else {
        dataText = document.getElementsByClassName('section-8')[0].querySelectorAll('[data-textmobile]');
        dataTextDynamic = document.getElementsByClassName('section-8')[0].querySelectorAll('[data-text]');
        blockChangeMobile();
        blockChangeMobileMain();
    }

    window.onresize = function () {
        if (window.innerWidth >= '921') {
            dataText = document.getElementsByClassName('section-8')[0].querySelectorAll('[data-text]');
            dataTextDynamic = document.getElementsByClassName('section-8')[0].querySelectorAll('[data-textmobile]');
            blockChange();
        } else if (window.innerWidth <= '920') {
            dataText = document.getElementsByClassName('section-8')[0].querySelectorAll('[data-textmobile]');
            dataTextDynamic = document.getElementsByClassName('section-8')[0].querySelectorAll('[data-text]');
            blockChangeMobile();
        }
    };


    function blockChangeMain() {
        for (var i = 0, m = dataOpen; i < m.length; i++) {
            (function () {
                var index = [i];
                var button = m;
                dataTextDynamic[index].style.display = 'none';

                button[i].addEventListener('click', function () {
                    [].forEach.call(button, function (callback) {
                        callback.classList.remove('gray');
                        callback.classList.add('imageright');
                    });

                    [].forEach.call(dataText, function (callback) {
                        callback.style.display = 'none';
                    });

                    button[index].classList.remove('imageright');
                    button[index].classList.add('gray');
                    dataText[index].style.display = 'block';

                    activeBlockNum = button[index].dataset.open;
                });
            }());
        }
    }

    function blockChangeMobileMain() {
        for (var i = 0, m = dataOpen; i < m.length; i++) {
            (function () {
                var index = [i];
                var button = m;
                dataTextDynamic[index].style.display = 'none';

                button[index].addEventListener('click', function () {

                        if (hasClass(button[index], 'gray')) {
                            button[index].classList.remove('gray');
                            button[index].classList.add('imageright');
                            dataText[index].style.display = 'none';
                            button[index].classList.remove('imagedown');
                        } else {
                            [].forEach.call(button, function (callback) {
                                callback.classList.remove('gray');
                                callback.classList.add('imageright');
                                callback.classList.remove('imagedown');
                            });

                            [].forEach.call(dataText, function (callback) {
                                callback.style.display = 'none';
                            });

                            button[index].classList.add('gray');
                            button[index].classList.remove('imageright');
                            dataText[index].style.display = 'block';
                            button[index].classList.add('imagedown');
                        }

                        activeBlockNum = button[index].dataset.open;
                    }
                );
            }());
        }
    }

    function blockChangeMobile() {
        if (!isNaN(activeBlockNum)) {
            dataOpen[activeBlockNum].classList.add('gray');
            dataOpen[activeBlockNum].classList.add('imageright');
            dataText[activeBlockNum].style.display = 'block';
            dataOpen[activeBlockNum].classList.add('imagedown');
        }
    }

    function blockChange() {
        var textElementsMobile = document.getElementsByClassName('section-8')[0].querySelectorAll('[data-textmobile]');
        var textElements = document.getElementsByClassName('section-8')[0].querySelectorAll('[data-text]');
        textElementsMobile.forEach(function (elemMob, index) {
            textElements[index].getElementsByTagName('p')[0].innerHTML = elemMob.getElementsByTagName('p')[0].innerHTML;

        });

        if (!isNaN(activeBlockNum)) {
            dataOpen[activeBlockNum].classList.add('gray');
            dataOpen[activeBlockNum].classList.remove('imageright');
            dataText[activeBlockNum].style.display = 'block';
        }
    }

}());
