'use strict';

const $ = require('jquery');
const config = require('../../../../../config');
// require('./device.min');
require('../../../scripts/vendor/bootstrap.min');
require('./inject_2.js');


const currencyRates = {
    // 'au': 1.35702,
    // 'ae': 3.67315,
    // 'ca': 1.29041,
    // 'gb': 0.692400,
    // 'ie': 0.876156,
    // 'nz': 1.46946,
    'us': 1
};

const mobileClasses = [
    'tablet',
    'mobile',
    'android',
    'blackberry',
    'fxos',
    'windows',
    'meego',
    'ios',
    'ipad',
    'iphone',
    'ipod',
];

export function init(locale, selector = '.calculator-theme__white', index = 1) {
    const currencyRate = currencyRates[locale];
    (function (selector, index) {
        let isMobile = false;
        let htmlClasses = document.documentElement.classList;
        mobileClasses.forEach(function (cls) {
            if (htmlClasses.contains(cls)) {
                isMobile = true;
            }
        });

        $(selector).find('.calculator_header .dropdown-menu li').click(function () {
            let text = $(this).text();
            let value = $(this).attr('data-calc-value');
            $(this).parents('.dropdown').find('.selected-value').html(text);
            $(this).parents('.dropdown').find('.selected-value').attr('data-calc-value', value);
            calculate();
        });

        let calculate = function () {
            if (!isMobile) {
                $(selector).find(`#calculator_service_${index}`).val(
                    parseInt($(selector).find(`#calculator_service_selected_${index} .selected-value`).attr('data-calc-value'))
                );
                $(selector).find(`#calculator_type_${index}`).val(
                    parseInt($(selector).find(`#calculator_type_selected_${index} .selected-value`).attr('data-calc-value'))
                );
                $(selector).find(`#calculator_deadline_${index}`).val(
                    parseInt($(selector).find(`#calculator_deadline_selected_${index} .selected-value`).attr('data-calc-value'))
                );
                $(selector).find(`#calculator_pages_${index}`).val(
                    parseInt($(selector).find(`#calculator_pages_selected_${index} .selected-value`).attr('data-calc-value'))
                );
            }

            let service = $(selector).find(`#calculator_service_${index} option:selected`).val();
            let type = $(selector).find(`#calculator_type_${index} option:selected`).val();
            let deadline = $(selector).find(`#calculator_deadline_${index} option:selected`).val();
            let pages = $(selector).find(`#calculator_pages_${index} option:selected`).val();

            if (service && type && deadline && pages) {
                $.ajax({
                    url: `https://app.essaypro.com/api/price/calculate`,
                    method: 'GET',
                    cache: true,
                    xhrFields: {
                        withCredentials: true
                    },
                    data: {
                        service: service,
                        type: type,
                        deadline: deadline,
                        pages: pages
                    }
                }).done(function (data) {
                    if (data.price && $(".calculator-theme__white").hasClass('additional__prices')) {
                        $(".additional__prices").find(`#calculator_total_${index}`).text((parseInt(data.price) * currencyRate).toFixed());
                        $(".additional__prices").find(`#calculator_total_plus_${index}`).text((parseInt(data.price) * 1.4).toFixed());
                    } else if (data.price && $(".calculator-theme__white").hasClass('words__min__price')) {
                        $(".words__minprice").find(`#calculator_total_price_${index}`).text((parseInt(data.price) * currencyRate).toFixed());
                    } else if (data.price) {
                        $(selector).find(`#calculator_total_${index}`).text((parseInt(data.price) * currencyRate).toFixed());
                    }
                });
            }

            //delete pages string
            if ($(".calculator-theme__white").hasClass('words__min__price') || $(".calculator-theme__white").hasClass('additional__prices')) {

                var horizontalCalc = document.getElementsByClassName('white__horizontal')[0] ? document.getElementsByClassName('white__horizontal')[0] : document.getElementsByClassName('additional__prices')[0];
                var elements = horizontalCalc.getElementsByClassName('calculator__form-row')[3].getElementsByClassName('calculator__form-field-group')[0].getElementsByClassName('calculator__form-field')[1].getElementsByClassName('dropdown-menu__list')[0].getElementsByClassName('dropdown-menu__item');
                var select = horizontalCalc.getElementsByClassName('calculator__form-row')[3].getElementsByClassName('calculator__form-field-group')[0].getElementsByClassName('calculator__form-field')[1].getElementsByClassName('select')[0].getElementsByClassName('calculator__form-select-half')[0].getElementsByTagName('option');
                var selectedValue = horizontalCalc.getElementsByClassName('calculator__form-row')[3].getElementsByClassName('calculator__form-field-group')[0].getElementsByClassName('calculator__form-field')[1].getElementsByClassName('dropdown')[0].getElementsByClassName('selected-value')[0];

                selectedValue.textContent = selectedValue.textContent.replace(/pages?/g, "");

                for (var i = 0; i < elements.length; i++) {
                    var element = elements[i];
                    elements[i].textContent = elements[i].textContent.replace(/pages?/g, "");
                    select[i].textContent = elements[i].textContent.replace(/pages?/g, "");
                }

            }
        };


        $(selector).find('.calculator select').change(function () {
            calculate();
        });


        $(selector).find(`#calculator_pages_increment_${index}`).click(function () {
            let newValue = 1;
            let nextOption = 1;
            if (isMobile) {
                nextOption = document.getElementById(`calculator_pages_${index}`).selectedIndex + 1;
                newValue = (nextOption == $(selector).find(`select#calculator_pages_${index} option`).length) ? 0 : nextOption;
            } else {
                nextOption = document.getElementById(`calculator_pages_${index}`).selectedIndex + 1;
                newValue = (nextOption == $(selector).find(`select#calculator_pages_${index} option`).length) ? 1 : nextOption + 1;
            }

            let el = $(`#pages_fields_${index} .dropdown-menu[aria-labelledby="calculator_pages_selected_${index}"] li[data-calc-value="${newValue}"]`).first();
            $(selector).find(`#calculator_pages_selected_${index} .selected-value`).html(el.text());
            $(selector).find(`#calculator_pages_selected_${index} .selected-value`).attr('data-calc-value', el.attr('data-calc-value'));

            $(selector).find(`select#calculator_pages_${index} option`).eq(newValue).prop('selected', true);
            calculate();
        });

        $(selector).find(`#calculator_pages_decrement_${index}`).click(function () {
            let prevOption = $(selector).find(`select#calculator_pages_${index} option`).length;
            let newValue = 1;
            if (isMobile) {
                prevOption = document.getElementById(`calculator_pages_${index}`).selectedIndex;
                newValue = (prevOption == 0) ? $(selector).find(`select#calculator_pages_${index} option`).length - 1 : prevOption - 1;
            } else {
                prevOption = document.getElementById(`calculator_pages_${index}`).selectedIndex + 1;
                newValue = (prevOption == 1) ? $(selector).find(`select#calculator_pages_${index} option`).length : prevOption - 1;
            }

            let el = $(`#pages_fields_${index} .dropdown-menu[aria-labelledby="calculator_pages_selected_${index}"] li[data-calc-value="${newValue}"]`).first();
            $(selector).find(`#calculator_pages_selected_${index} .selected-value`).html(el.text());
            $(selector).find(`#calculator_pages_selected_${index} .selected-value`).attr('data-calc-value', el.attr('data-calc-value'));

            $(selector).find(`select#calculator_pages_${index} option`).eq(newValue).prop('selected', true);
            calculate();
        });

        calculate();
    })(selector, index);
}
