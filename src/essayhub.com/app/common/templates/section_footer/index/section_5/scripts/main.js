$(function () {
    $('[data-clip]').on('click', function () {
        var el = $(this).data('clip');
        var text = $(this).text();
        $('[data-clipped=' + el + ']').toggleClass('clip');
        $(this).text(toggleText(text));
    });
    function toggleText(text) {
        var readMore = 'Read more';
        var hide = 'Hide';

        text = text || hide;

        if (text == hide) {
            return readMore;
        }

        return hide;
    }
});
