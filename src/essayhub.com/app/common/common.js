'use strict';

require('./locale');

let calc = require('./../common/templates/calculator/calc/calc__additional__prices');

calc.init('us');


let inject = require('./inject');
inject();

require('../common/main');

if (location.pathname.length > 1 && !location.pathname.match(/index.html/)) {
    window.onscroll = function () {
        var sy, d = document, r = d.documentElement, b = d.body;
        sy = r.scrollTop || b.scrollTop || 0;

        var isModified;
        if (sy > 160) {
            isModified = (sy === 60);
            if (!$('#nav-mob').hasClass('show-nav') && !document.getElementById('header').className.match(/header__white/g)) {
                document.getElementById('header').className += ' header__white';
                $('[data-logo="white"]').toggleClass('logo_hidden', !isModified);
                $('[data-logo="black"]').toggleClass('logo_hidden', isModified);
            }
        } else {
            isModified = (sy === 60);
            document.getElementById('header').className = document.getElementById('header').className.replace(/header__white/g, '');
            $('[data-logo="white"]').toggleClass('logo_hidden', isModified);
            $('[data-logo="black"]').toggleClass('logo_hidden', !isModified);
        }
    };

}

$('#learn').on('click', function (event) {
    event.stopPropagation();
    $('.mdl-menu__container').toggleClass('is-visible');
});

$(document).on('click', function () {
    $('.mdl-menu__container').removeClass('is-visible');
});

$('.toggle_nav-mob').click(function (e) {
    e.preventDefault();

    $('#nav-mob').toggleClass('show-nav');
    $('.nav_mob-i').toggleClass('onClose_i');
    if ($('#nav-mob').hasClass('show-nav')) {
        $("body").append("<div class='mfp-bg'></div>");
        $("body").find(".auth").css({"display": "none"});
        $("body").find(".header__not-fixed").css({"background-color": "transparent"});
        $("body").find(".header__not-fixed").css({"boxShadow": "none"});
    } else {
        $("body").find(".mfp-bg").remove();
        $("body").find(".auth").css({"display": "block"});
        document.getElementsByClassName('header__not-fixed')[0].removeAttribute('style');
    }

});

$("body").click(function (e) {
    $(".mfp-bg").click(function (e) {
        if (this.className === 'mfp-bg') {
            $('#nav-mob').removeClass('show-nav');
            $('.nav_mob-i').removeClass('onClose_i');
            $("body").find(".mfp-bg").remove();
            $("body").find(".auth").css({"display": "block"});
            document.getElementsByClassName('header__not-fixed')[0].removeAttribute('style');
        }
    });
});


