$(document).on('scroll', function () {
    var firstSection = $('.section-1');
    var isModified = $(this).scrollTop() - firstSection.height() >= 0;
    if (location.pathname.length <= 1 || location.pathname.match(/index.html/)) {

        $('[data-logo="white"]').toggleClass('logo_hidden', isModified);
        $('[data-logo="black"]').toggleClass('logo_hidden', !isModified);
        $('#header').toggleClass('header__white', isModified);
    }
});

$(window).on('resize', function () {
    var width = $(window).width();

    if ($(this).width() <= 540) {
        $(document).on('scroll', function () {
            var firstSection = $('.section-1');
            var isModified = $(this).scrollTop() - firstSection.height() >= 0;

            $('[data-logo="white"]').toggleClass('logo_hidden', isModified);
            $('[data-logo="black"]').toggleClass('logo_hidden', !isModified);
            $('#header').toggleClass('header__white', isModified);
        });
    }
});
