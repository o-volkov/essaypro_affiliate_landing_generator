'use strict';

require('./inject');



require('../../common/templates/index/section_3/scripts/main');
require('../../common/templates/index/section_5/scripts/main');
require('../../common/templates/index/section_8/scripts/main');

require('../../common/main.js');


require('retinajs');
require('magnific-popup/dist/jquery.magnific-popup.min.js');


function init(locale) {
    (function (locale) {
        $(document).ready(function() {
            retinajs($('img[data-rjs]'));

            try {
                $('.with-popup').magnificPopup({
                    type: 'image',
                    closeOnContentClick: true,
                    closeBtnInside: false,
                    fixedContentPos: true,
                    mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
                    image: {
                        verticalFit: true
                    },
                    zoom: {
                        enabled: true,
                        duration: 300 // don't foget to change the duration also in CSS
                    }
                });
            } catch (e) {
            }
        });
    })(locale)
}

init('us');
