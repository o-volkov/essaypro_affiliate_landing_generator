'use strict';

const HttpDispatcher = require('httpdispatcher');
const http = require('http');
const path = require('path');
const fileSystem = require('fs');
const url = require('url');
const qs = require('querystring');
const spawn = require('child_process').spawn;

var dispatcher = new HttpDispatcher();

const HOST = '127.0.0.1';
const PORT = 8181;

dispatcher.setStaticDirname(__dirname);
dispatcher.setStatic('resources');
dispatcher.setStaticDirname('server/builds');
dispatcher.onGet('/', function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end(`
<form method="get" action="/build">
  <label for="tap_s">Set tap_s:</label>
  <input id="tap_s" name="tap_s" required="required" />
  <label for="site">Select site:</label>
  <select id="select" name="site" required="required">
    <option value="com" selected="selected">essaypro.com</option>
    <option value="grade">essayhub.com</option>
    <option value="help">essayservice.com</option>
  </select>
  <button type="submit">Send</button>
</form>
  `);
});

dispatcher.onGet('/build', function (req, res) {
    const query = qs.parse(url.parse(req.url).query);
    const tap_s = query.tap_s;
    const site = query.site;

    const webpack = spawn(`npm`, ['run', `build:${site}`], {
        'env': {
            'TAP_S': tap_s,
            'PATH': `${__dirname}/node_modules/.bin:${process.env.PATH}`,
            'NODE_ENV': 'production'
        }
    });

    webpack.stdout.on('data', (data) => {
    });

    webpack.stderr.on('data', (data) => {
        console.error(`Webpack ERROR: ${data}`);
    });

    webpack.on('close', (code) => {
        if (code == 0) {
            const zip = spawn('zip', ['-r', `dist_${site}_${tap_s}.zip`, `dist_${tap_s}`], {'cwd': `${__dirname}/server/builds/`});

            zip.stderr.on('data', (data) => {
                console.error(`ZIP ERROR: ${data}`);
            });

            zip.on('close', (zipCode) => {
                if (zipCode == 0) {
                    let filePath = `${__dirname}/server/builds/dist_${site}_${tap_s}.zip`;
                    var stat = fileSystem.statSync(filePath);

                    res.writeHead(200, {
                        'Content-Disposition': `form-data; name="dist_${site}_${tap_s}.zip"; filename="dist_${site}_${tap_s}.zip"`,
                        'Content-Type': 'application/zip',
                        'Content-Length': stat.size
                    });

                    var readStream = fileSystem.createReadStream(filePath);
                    readStream.pipe(res);
                    return 0;
                } else {
                    console.error(`Zip not 0 code on exit: ${zipCode}!`);
                    return res.end('ERROR');
                }
            });
        } else {
            console.error(`Webpack not 0 code on exit: ${code}!`);
            return res.end('ERROR');
        }
    });
});

dispatcher.onError(function (req, res) {
    res.writeHead(404);
});

http.createServer(function (req, res) {
    try {
        dispatcher.dispatch(req, res);
    } catch (e) {
        console.error(e);
    }
}).listen(PORT, HOST);


function moduleExists(name) {
    try {
        return require.resolve(name)
    } catch (e) {
        return false
    }
}

if (moduleExists('opn')) {
    const opn = require('opn');
    opn(`http://${HOST}:${PORT}/`);
}
