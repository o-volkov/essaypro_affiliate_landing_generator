'use strict';

let path = require('path');
let fsSync = require('fs-sync');

let webpack = require('webpack');
let HtmlWebpackPlugin = require('html-webpack-plugin');
let ExtractTextPlugin = require('extract-text-webpack-plugin');
let CopyWebpackPlugin = require('copy-webpack-plugin');

let rimraf = require('rimraf');

const IS_TEST = process.env.TEST || false;
const ESSAY_ENV = process.env.ESSAY_ENV || 'production';
const NODE_ENV = process.env.NODE_ENV || 'development';
const HOST = process.env.HOST || '127.0.0.1';
const PORT = process.env.PORT || 8080;

const TAP_S = process.env.TAP_S || '1234-abcdef';

const TEMPLATE_EXTENSION = 'ejs';
const DEFAULT_LOCALES = [
  {url: "us", hreflang: "en"}
];

const context = path.join(__dirname, 'src/essayhub.com');

if (fsSync.exists(path.join(__dirname, 'src/essayhub.com/config_' + ESSAY_ENV + '.js'))) {
  fsSync.copy(
    (path.join(__dirname, 'src/essayhub.com/config_' + ESSAY_ENV + '.js')),
    (path.join(__dirname, 'src/essayhub.com/config.js')),
    {force: true}
  );
}

let config = require('./src/essayhub.com/config');
let pages = require('./src/essayhub.com/pages.json');

function moduleExists(name) {
  try {
    return require.resolve(name)
  } catch (e) {
    return false
  }
}

let entries = {};

Object.keys(pages).forEach(page => {
  if (!pages[page]['locales']) {
    pages[page].locales = DEFAULT_LOCALES
  }
  pages[page].locales.forEach(locale => {
    entries[`${locale.url}_${page}`] = `./app/${locale.url}/${page}/${page}`;
  });
});

entries[`common`] = `./app/common/common`;


let options = {
  context: context,

  target: 'web',

  entry: entries,

  output: {
    path: path.join(`${path.join(path.join(__dirname, 'server'), 'builds')}`, `dist_${TAP_S}`),
    publicPath: '/',
    filename: '[name].[hash:6].js',
    library: '[name]'
  },

  resolve: {
    modulesDirectories: ['node_modules', context],
    extensions: [
      '.js',
      '.json',
      '.css',
      '.scss',
      '.html',
      '.ejs',
      ''
    ]
  },

  resolveLoader: {
    modulesDirectories: ['node_modules'],
    modulesTemplates: ['*-loader', '*'],
    extensions: ['', '.js']
  },

  // =====================================
  module: {
    preLoaders: [],

    loaders: [
      {
        test: /\.js$/i,
        exclude: /node_modules/,
        loader: 'babel?presets[]=es2015'
      },
      {
        test: /\.json$/i,
        exclude: /node_modules/,
        loader: 'json'
      },
      {
        test: /\.html$/i,
        loader: 'html'
      },
      {
        test: /\.ejs$/i,
        exclude: /node_modules/,
        loader: 'ejs?variable=data'
      },
      {
        test: /\.(gif)$/i,
        loader: 'file'
      },
      {
        test: /\.svg/i,
        loader: 'file'
      },
      {
        test: /\.png/i,
        loader: 'file?mimetype=image/png'
      },
      {
        test: /\.jpeg$/i,
        loaders: ['file?mimetype=image/jpg', 'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false']
      },
      {
        test: /\.jpg$/i,
        loaders: ['file?mimetype=image/jpg', 'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false']
      },
      {
        test: /\.(otf|eot|ttf|woff|woff2)$/i,
        loader: 'url'
      },
      {
        test: /materialize-css\/bin\//,
        loader: 'imports?jQuery=jquery,$=jquery,hammerjs'
      },
      {
        test: /\.(mp4|ogg)$/,
        loader: 'file-loader',
      }
    ]
  },

  // =====================================
  plugins: [
    {
      apply: (compiler) => rimraf.sync(compiler.options.output.path)
    },

    new webpack.optimize.CommonsChunkPlugin({
      filename: 'common.js',
      name: 'common'
    }),

    new webpack.ProvidePlugin({
      _: 'underscore',
      $: 'jquery',
      jQuery: 'jquery'
    }),

    new webpack.DefinePlugin({
      NODE_ENV: JSON.stringify(NODE_ENV),
      TAP_S: JSON.stringify(TAP_S)
    }),

    new CopyWebpackPlugin([
      // { from: './shared/images/icons', to: 'shared/images/icons' },
      { from: './shared/images/og', to: 'shared/images/og' },
      { from: './favicon.ico' },
      { from: './robots.txt' }
    ]),

    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en-au|en-gb/)
  ],

  noParse: [
    /[\/\\]src[\/\\]app[\/\\]essayhub.com[\/\\]common[\/\\]scripts[\/\\]materialize-css[\/\\]materialize\.js$/,
    /[\/\\]node_modules[\/\\]jquery[\/\\]dist\.js$/,
    /[\/\\]node_modules[\/\\]moment[\/\\]locale\.js$/,
    /[\/\\]node_modules[\/\\]html-minifier[\/\\]dist\.js$/,
    /[\/\\]node_modules[\/\\]html-minifier[\/\\]src\.js$/,
  ]
};

// =====================================
let htmlWebpackPluginConfig = {
  frontend_url: config.frontend_url,
  backend_url: config.backend_url,
  tap_s: TAP_S
};

if (NODE_ENV == 'development') {
  htmlWebpackPluginConfig.minify = false;
  htmlWebpackPluginConfig.hash = false;
  htmlWebpackPluginConfig.cache = false;
  htmlWebpackPluginConfig.showErrors = true;

  options.watch = true;
  options.watchOptions = {
    aggregateTimeout: 100
  };

  options.devServer = {
    devtool: 'source-map',
    contentBase: 'dist',
    stats: {
      modules: false,
      cached: false,
      colors: true,
      chunk: false
    },
    host: HOST,
    port: PORT
  };

  let extractCSS = new ExtractTextPlugin('[name].css');

  options.module.loaders.push({
    test: /\.css$/i,
    loader: extractCSS.extract('style', 'css?minimize')
  });

  options.module.loaders.push(  {
    test: /\.scss$/i,
    loader: extractCSS.extract('style', 'css?sourceMap!sass?sourceMap')
  });

  options.plugins.push(extractCSS);

  options.module.exprContextCritical = false;
  options.output.filename = '[name].js';
  if (process.env.NODE_PROFILE) {
    options.debug = true;
    options.profile = true;
  }
} else if (NODE_ENV == 'production') {
  htmlWebpackPluginConfig.minify = {
    removeComments: true,
    collapseWhitespace: true,
    conservativeCollapse: true,
    collapseInlineTagWhitespace: true
  };
  htmlWebpackPluginConfig.hash = false;
  htmlWebpackPluginConfig.cache = true;
  htmlWebpackPluginConfig.showErrors = false;

  let extractCSS = new ExtractTextPlugin('[name].css');

  options.module.loaders.push({
    test: /\.css$/i,
    loader: extractCSS.extract('style', 'css')
  });

  options.module.loaders.push(  {
    test: /\.scss$/i,
    loader: extractCSS.extract('style', 'css?sourceMap!sass?sourceMap')
  });

  options.plugins.push(extractCSS);

  options.plugins.push(new webpack.optimize.OccurenceOrderPlugin());
  options.plugins.push(new webpack.optimize.DedupePlugin());
  options.plugins.push(new webpack.NoErrorsPlugin());
  options.plugins.push(new webpack.optimize.UglifyJsPlugin({minimize: true}));
}


Object.keys(pages).forEach(page => {
  if (!pages[page]['locales']) {
    pages[page].locales = DEFAULT_LOCALES
  }

  pages[page].locales.forEach(locale => {
    htmlWebpackPluginConfig.template = `./app/${locale.url}/${page}/${page}.${TEMPLATE_EXTENSION}`;
    let filename = pages[page].filename;
    if (locale.url !== 'us') {
      filename = `${locale.url}/${filename}`;
    }
    htmlWebpackPluginConfig.filename = `${filename}`;
    htmlWebpackPluginConfig.chunks = [`${locale.url}_${page}`];
    options.plugins.push(new HtmlWebpackPlugin(htmlWebpackPluginConfig));
  });
});

if (moduleExists('open-browser-webpack-plugin') && IS_TEST) {
  options.plugins.push(new (require('open-browser-webpack-plugin'))({url: 'http://' + HOST + ':' + PORT + options.output.publicPath}));
}

if (moduleExists('unused-files-webpack-plugin')) {
  options.plugins.push(new (require('unused-files-webpack-plugin').default)());
}

module.exports = options;
